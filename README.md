# Simulator-of-drones

This is a project of cours _Process and quality of software_.

You can use it to simulate the drones in a city. The drones will move automatically in the city and complete some tasks. For example, go to the docking station and charge of the battery, picking the goods, etc.
