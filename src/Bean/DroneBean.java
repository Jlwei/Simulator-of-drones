package Bean;

import components.Drone;

import java.util.ArrayList;

public class DroneBean {
    private Drone drone;
    private int x;
    private int y;
    private int scoreXX;
    private int scoreXY;
    private int scoreYY;
    private int scoreYX;
    private Boolean free;
    private Boolean charge;
    private int ele;
    private ArrayList<Integer> arrayList;
    private int arrayX;
    private int arrayY;
    private int BuildingNumber;

    //The coordinates to the base station and the correlation
    private ArrayList<Integer> arrayList2;
    private int prox;       // It's also 6 steps
    private int proy;
    private Boolean free2;      // The priority of this bool is greater than the other bool
    private StationBean stationBean;        // Finally, the base station is set to a free state

    private Boolean free3;          // true = Representative plant, false = building
    private PlantBean plantBean;

    public Boolean getFree3() {
        return free3;
    }

    public void setFree3(Boolean free3) {
        this.free3 = free3;
    }

    public PlantBean getPlantBean() {
        return plantBean;
    }

    public void setPlantBean(PlantBean plantBean) {
        this.plantBean = plantBean;
    }

    public StationBean getStationBean() {
        return stationBean;
    }

    public void setStationBean(StationBean stationBean) {
        this.stationBean = stationBean;
    }

    public ArrayList<Integer> getArrayList2() {
        return arrayList2;
    }

    public void setArrayList2(ArrayList<Integer> arrayList2) {
        this.arrayList2 = arrayList2;
    }

    public int getProx() {
        return prox;
    }

    public void setProx(int prox) {
        this.prox = prox;
    }

    public int getProy() {
        return proy;
    }

    public void setProy(int proy) {
        this.proy = proy;
    }

    public Boolean getFree2() {
        return free2;
    }

    public void setFree2(Boolean free2) {
        this.free2 = free2;
    }

    public int getBuildingNumber() {
        return BuildingNumber;
    }

    public void setBuildingNumber(int buildingNumber) {
        BuildingNumber = buildingNumber;
    }

    public int getArrayX() {
        return arrayX;
    }

    public void setArrayX(int arrayX) {
        this.arrayX = arrayX;
    }

    public int getArrayY() {
        return arrayY;
    }

    public void setArrayY(int arrayY) {
        this.arrayY = arrayY;
    }

    public ArrayList<Integer> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Integer> arrayList) {
        this.arrayList = arrayList;
    }

    public int getEle() {
        return ele;
    }

    public void setEle(int ele) {
        this.ele = ele;
    }

    public Boolean getCharge() {
        return charge;
    }

    public void setCharge(Boolean charge) {
        this.charge = charge;
    }

    public Boolean getFree() {
        return free;
    }

    public int getScoreXX() {
        return scoreXX;
    }

    public void setScoreXX(int scoreXX) {
        this.scoreXX = scoreXX;
    }

    public int getScoreXY() {
        return scoreXY;
    }

    public void setScoreXY(int scoreXY) {
        this.scoreXY = scoreXY;
    }

    public int getScoreYY() {
        return scoreYY;
    }

    public void setScoreYY(int scoreYY) {
        this.scoreYY = scoreYY;
    }

    public int getScoreYX() {
        return scoreYX;
    }

    public void setScoreYX(int scoreYx) {
        this.scoreYX = scoreYx;
    }

    public void setFree(Boolean free) {
        this.free = free;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public DroneBean(Drone drone) {
        this.drone = drone;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }
}
