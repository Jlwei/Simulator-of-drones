package Bean;

import components.Building;

public class BuildingBean {
    private Building building;
    private Integer x;
    private Integer y;
    private Boolean judge;
    private GoodsBean haveGoods;
    private Boolean havePlane;

    public Boolean getHavePlane() {
        return havePlane;
    }

    public void setHavePlane(Boolean havePlane) {
        this.havePlane = havePlane;
    }

    public GoodsBean getHaveGoods() {
        return haveGoods;
    }

    public void setHaveGoods(GoodsBean haveGoods) {
        this.haveGoods = haveGoods;
    }

    public Boolean getJudge() {
        return judge;
    }

    public void setJudge(Boolean judge) {
        this.judge = judge;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Building getBuilding() {
        return building;
    }

    void setBuilding(Building building) {
        this.building = building;
    }

    public BuildingBean(Building building) {
        this.building = building;
    }
}
