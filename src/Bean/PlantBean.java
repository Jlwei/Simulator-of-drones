package Bean;

import components.Plant;
import javafx.collections.ObservableList;

public class PlantBean {
    private Plant plant;
    private int x;
    private int y;
    private Boolean color;       //red true  green false
    private Boolean havePlant;

    public Boolean getHavePlant() {
        return havePlant;
    }

    public void setHavePlant(Boolean havePlant) {
        this.havePlant = havePlant;
    }

    public Boolean getColor() {
        return color;
    }

    public void setColor(Boolean color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public PlantBean(Plant plant) {
        this.plant = plant;
    }

    public Plant getPlant() {
        return plant;
    }

    public void setPlant(Plant plant) {
        this.plant = plant;
    }

    public void setPlante(int x, int y) {
        ObservableList<Double> list = plant.getPolygon().getPoints();
        list.clear();
        plant.getPolygon().getPoints().addAll(new Double[]{(double) x - 15, (double) y, (double) x + 15, (double) y, (double) x, (double) y - 15 * Math.sqrt(3)});
    }
}
