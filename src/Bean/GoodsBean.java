package Bean;

import components.Goods;

public class GoodsBean {
    private Goods goods;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public GoodsBean(Goods goods) {

        this.goods = goods;
    }
}
