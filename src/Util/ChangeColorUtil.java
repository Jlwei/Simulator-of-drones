package Util;

import Bean.DroneBean;
import Bean.PlantBean;
import layout.AboutLeft;
import layout.Run;

import java.util.ArrayList;

public class ChangeColorUtil {
    private static int domain = 0;

    public static void changeColor(ArrayList<PlantBean> list) {
        //Find the nearest drone for every plant.
        for (int i = 0; i < list.size(); i++) {
            PlantBean plantBean = list.get(i);

            //find the free drone
            int number = -1, dom = 0, judge = 0;     //The number of the nearest drone
            double distance = 0, distance2 = 0;
            for (int i2 = 0; i2 < AboutLeft.droneBeanArrayList.size(); i2++) {
                DroneBean droneBean = AboutLeft.droneBeanArrayList.get(i2);

                if (droneBean.getScoreXX() < plantBean.getX() && droneBean.getScoreXY() > plantBean.getX()
                        && droneBean.getScoreYX() < plantBean.getY() && droneBean.getScoreYY() > plantBean.getY()) {
//                    System.out.println(" " + droneBean.getFree() + " " + plantBean.getHavePlant() + " " + droneBean.getFree2());
                    if (droneBean.getFree() == false && plantBean.getHavePlant() == false && droneBean.getFree2() == false) {
                        //Calculate the distance
                        distance = getDistance(plantBean, droneBean);
                        if (judge == 0 || distance2 > distance) {
                            distance2 = distance;
                            number = i2;
                            dom = domain;
                            judge = 1;
                        }
                    }
                }
            }
//            System.out.println("number:  " + number);
            if (number != -1) {
                //Let the drone go to the plant
                Run run = new Run(AboutLeft.droneBeanArrayList.get(number).getX(), AboutLeft.droneBeanArrayList.get(number).getY(), plantBean.getX(), plantBean.getY(), dom);
                ArrayList<Integer> arrayList = run.move(false);

                AboutLeft.droneBeanArrayList.get(number).setFree(true);
                AboutLeft.droneBeanArrayList.get(number).setFree3(true);
                AboutLeft.droneBeanArrayList.get(number).setArrayX(0);
                AboutLeft.droneBeanArrayList.get(number).setArrayList(arrayList);
                AboutLeft.droneBeanArrayList.get(number).setArrayY(1);
                plantBean.setHavePlant(true);
                AboutLeft.droneBeanArrayList.get(number).setPlantBean(plantBean);
            }
        }
    }

    public static double getDistance(PlantBean plantBean, DroneBean droneBean) {
        if (droneBean.getX() == plantBean.getX()) {
            if (droneBean.getY() > plantBean.getY()) {
                domain = 0;
                return droneBean.getY() - plantBean.getY();
            } else {
                domain = 6;
                return plantBean.getY() - droneBean.getY();
            }
        } else if (droneBean.getX() == plantBean.getX()) {
            if (droneBean.getY() > plantBean.getY()) {
                domain = 1;
                return droneBean.getY() - plantBean.getY();
            } else {
                domain = 7;
                return plantBean.getY() - droneBean.getY();
            }
        } else if (droneBean.getX() > plantBean.getX() && droneBean.getY() > plantBean.getY()) {
            domain = 2;

            return DistanceUtil.getDistance(droneBean.getX() - plantBean.getX(), droneBean.getY() - plantBean.getY());
        } else if (droneBean.getX() > plantBean.getX() && droneBean.getY() < plantBean.getY()) {
            domain = 3;
            return DistanceUtil.getDistance(droneBean.getX() - plantBean.getX(), plantBean.getY() - droneBean.getY());
        } else if (droneBean.getX() < plantBean.getX() && droneBean.getY() < plantBean.getY()) {
            domain = 4;
            return DistanceUtil.getDistance(plantBean.getX() - droneBean.getX(), plantBean.getY() - droneBean.getY());
        } else {
            domain = 5;
            return DistanceUtil.getDistance(plantBean.getX() - droneBean.getX(), droneBean.getY() - plantBean.getY());
        }
    }
}
