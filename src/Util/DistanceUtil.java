package Util;

public class DistanceUtil {
    //The nearest distance
    public static double getDistance(int x, int y) {
        double _x = Math.abs(x);
        double _y = Math.abs(y);
        return Math.sqrt(_x * _x + _y * _y);
    }
}
