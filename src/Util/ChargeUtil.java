package Util;

import Bean.DroneBean;
import Bean.StationBean;
import layout.Run;

import java.util.ArrayList;

public class ChargeUtil {
    private static int domain = 0;

    public static void findAStation(DroneBean droneBean, ArrayList<StationBean> list) {
        ArrayList<StationBean> arrayList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            StationBean stationBean = list.get(i);
            //If the base station records the base station in the range of drone
            if (droneBean.getScoreXX() < stationBean.getX() && droneBean.getScoreXY() > stationBean.getX()
                    && droneBean.getScoreYX() < stationBean.getY() && droneBean.getScoreYY() > stationBean.getY()) {
                //If the base station is in idle state
                if (stationBean.getFree() == false) {
                    arrayList.add(stationBean);
                }
            }
        }
        double distance = 0, distance2 = 0;
        int number = 0, judge = 0;
        //Choose a suitable base station for charging in the base station.
        for (int i = 0; i < arrayList.size(); i++) {
            StationBean stationBean = arrayList.get(i);
            //Calculation distance
            distance = getDistance(stationBean, droneBean);
            if (judge == 0 || distance2 > distance) {
                number = i;
                distance2 = distance;
                judge = 1;
            }
        }

        //Get the coordinates of the 2 base stations that the nearest base station ran to the coordinates of the first 2 planes.
        if (arrayList.get(number) != null) {
            Run run = new Run(droneBean.getX(), droneBean.getY(), arrayList.get(number).getX(), arrayList.get(number).getY(), domain);
            ArrayList<Integer> proxy = run.move(true);
            droneBean.setFree2(true);
            droneBean.setArrayList2(proxy);
            droneBean.setProx(0);
            droneBean.setProy(1);
            droneBean.setFree(false);
            if (droneBean.getPlantBean() != null) {
                droneBean.getPlantBean().setHavePlant(false);
            }
            arrayList.get(number).setFree(true);
            droneBean.setStationBean(arrayList.get(number));
        }
    }

    public static double getDistance(StationBean stationBean, DroneBean droneBean) {
        if (droneBean.getX() == stationBean.getX()) {
            if (droneBean.getY() > stationBean.getY()) {
                domain = 0;
                return droneBean.getY() - stationBean.getY();
            } else {
                domain = 6;
                return stationBean.getY() - droneBean.getY();
            }
        } else if (droneBean.getX() == stationBean.getX()) {
            if (droneBean.getY() > stationBean.getY()) {
                domain = 1;
                return droneBean.getY() - stationBean.getY();
            } else {
                domain = 7;
                return stationBean.getY() - droneBean.getY();
            }
        } else if (droneBean.getX() > stationBean.getX() && droneBean.getY() > stationBean.getY()) {
            domain = 2;
            return DistanceUtil.getDistance(droneBean.getX() - stationBean.getX(), droneBean.getY() - stationBean.getY());
        } else if (droneBean.getX() > stationBean.getX() && droneBean.getY() < stationBean.getY()) {
            domain = 3;
            return DistanceUtil.getDistance(droneBean.getX() - stationBean.getX(), stationBean.getY() - droneBean.getY());
        } else if (droneBean.getX() < stationBean.getX() && droneBean.getY() < stationBean.getY()) {
            domain = 4;
            return DistanceUtil.getDistance(stationBean.getX() - droneBean.getX(), stationBean.getY() - droneBean.getY());
        } else {
            domain = 5;
            return DistanceUtil.getDistance(stationBean.getX() - droneBean.getX(), droneBean.getY() - stationBean.getY());
        }
    }
}
