package Util;

import Bean.BuildingBean;
import Bean.DroneBean;
import layout.AboutLeft;
import layout.Run;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CompareUtil {
    private static int domain = 0;

    public static void compare() {
        //Look at a few in the range
        for (int i2 = 0; i2 < AboutLeft.buildingBeanArrayList.size(); i2++) {
            BuildingBean buildingBean = AboutLeft.buildingBeanArrayList.get(i2);
            if (buildingBean.getJudge() == true && buildingBean.getHavePlane() == false) {
                ArrayList<DroneBean> arrayList = new ArrayList<>();
                for (int i = 0; i < AboutLeft.droneBeanArrayList.size(); i++) {
                    DroneBean droneBean = AboutLeft.droneBeanArrayList.get(i);
//                    System.out.println(AboutLeft.droneBeanArrayList.size());

//                    System.out.println(droneBean.getScoreXX() + " " + droneBean.getScoreXY() + " " + droneBean.getScoreYX() + " " + droneBean.getScoreYY() + " " + buildingBean.getX() + " " + buildingBean.getY());
                    if (droneBean.getScoreXX() < buildingBean.getX() && droneBean.getScoreXY() > buildingBean.getX()
                            && droneBean.getScoreYX() < buildingBean.getY() && droneBean.getScoreYY() > buildingBean.getY()) {
                        if (droneBean.getFree() == false) {
                            arrayList.add(droneBean);
                        }
                    }
                }

                if (arrayList.size() >= 1) {
                    double distance = 0;
                    int number = 0, domainReal = 0, judge = 0;
                    for (int i = 0; i < arrayList.size(); i++) {
                        double distance2 = getDomain(buildingBean, arrayList.get(i));
//                        System.out.println(distance2);
                        if (distance > distance2 || judge == 0) {
                            distance = distance2;
                            number = i;
                            domainReal = domain;
                            judge = 1;
                        }
                    }

                    DroneBean droneBean = arrayList.get(number);
                    Run run = new Run(arrayList.get(number).getX(), arrayList.get(number).getY(), buildingBean.getX(), buildingBean.getY(), domainReal);
                    ArrayList<Integer> arrayList2 = run.move(false);
                    droneBean.setArrayList(arrayList2);
                    droneBean.setArrayX(0);
                    droneBean.setArrayY(1);
                    droneBean.setFree(true);
                    droneBean.setFree3(false);
                    droneBean.setBuildingNumber(i2);
                    buildingBean.setHavePlane(true);
                }
            }
        }
    }


    //Get the distance
    public static double getDomain(BuildingBean buildingBean, DroneBean droneBean) {
        Map<Integer, Double> map = new HashMap<>();
        if (droneBean.getX() == buildingBean.getX()) {
            if (buildingBean.getY() - droneBean.getY() > 0) {
                domain = 0;
                return buildingBean.getY() - droneBean.getY();
            } else {
                domain = 6;
                return droneBean.getY() - buildingBean.getY();
            }
        } else if (droneBean.getY() == buildingBean.getY()) {
            if (buildingBean.getX() - droneBean.getX() > 0) {
                domain = 1;
                return buildingBean.getX() - droneBean.getX();
            } else {
                domain = 7;
                return droneBean.getX() - buildingBean.getX();
            }
        } else if (droneBean.getX() > buildingBean.getX() && droneBean.getY() > buildingBean.getY()) {
            domain = 2;
            return DistanceUtil.getDistance(droneBean.getX() - buildingBean.getX(), droneBean.getY() - buildingBean.getY());
        } else if (droneBean.getX() > buildingBean.getX() && droneBean.getY() < buildingBean.getY()) {
            domain = 3;
            return DistanceUtil.getDistance(droneBean.getX() - buildingBean.getX(), buildingBean.getY() - droneBean.getY());
        } else if (droneBean.getX() < buildingBean.getX() && droneBean.getY() < buildingBean.getY()) {
            domain = 4;
            return DistanceUtil.getDistance(buildingBean.getX() - droneBean.getX(), buildingBean.getY() - droneBean.getY());
        } else {
            domain = 5;
            return DistanceUtil.getDistance(buildingBean.getX() - droneBean.getX(), droneBean.getY() - buildingBean.getY());
        }
    }
}
