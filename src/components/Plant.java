package components;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class Plant extends Group {
    private Polygon polygon;

    public Plant(int id, double x, double y) {
        super();
        polygon = new Polygon();
		polygon.getPoints().addAll(new Double[] { x - 15, y, x + 15, y, x, y - 15 * Math.sqrt(3) });
//        ObservableList<Double> list = polygon.getPoints();
//        double centerX = x, centerY = y;
//        double R = 0.5 * 3 / Math.sin(Math.PI / 40);
//        for (int i = 0; i < 3; i++) {
//            list.add(centerX + R * Math.cos(Math.PI / 2 + i * 2 * Math.PI / 3));
//            list.add(centerY + R * Math.sin(Math.PI / 2 + i * 2 * Math.PI / 3));
//        }
        polygon.setFill(Color.GREEN);
        getChildren().add(polygon);
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }
}
