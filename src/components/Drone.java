package components;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Drone {
    private Circle circle;

    public Drone(float x, float y, float size) {
        circle = new Circle();
        circle.setCenterX(x);
        circle.setCenterY(y);
        circle.setRadius(size);
        circle.setFill(Color.WHITE);
        circle.setStroke(Color.BLACK);
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(int x, int y) {
        circle.setCenterX(x);
        circle.setCenterY(y);
    }


}
