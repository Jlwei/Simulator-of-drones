package components;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Building {
    private Rectangle r;

    public Building(Integer x, Integer y, Integer xSize, Integer ySize) {
        r = new Rectangle();
        r.setFill(Color.WHITE);
        r.setStroke(Color.BLACK);
        r.setX(x);
        r.setY(y);
        r.setWidth(xSize);
        r.setHeight(ySize);
    }

    public void setR(int x, int y) {
        r.setX(x);
        r.setY(y);
    }

    public Rectangle getR() {
        return r;
    }

    public void setR(Rectangle r) {
        this.r = r;
    }
}
