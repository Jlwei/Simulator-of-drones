package components;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Goods {
    private Circle circle;

    public Goods(float x, float y, float size, Color color) {
        circle = new Circle();
        circle.setCenterX(x);
        circle.setCenterY(y);
        circle.setRadius(size);
        circle.setFill(color);
        circle.setStroke(Color.BLACK);
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(int x, int y) {
        circle.setCenterX(x);
        circle.setCenterY(y);
    }
}
