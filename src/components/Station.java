package components;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Station {
    private Rectangle r;

    public Station(Integer x, Integer y, Integer xSize, Integer ySize, Color color) {
        r = new Rectangle();
        r.setFill(color);
        r.setStroke(Color.BLACK);
        r.setX(x);
        r.setY(y);
        r.setWidth(xSize);
        r.setHeight(ySize);
    }

    public void setR(int x, int y) {
        r.setX(x);
        r.setY(y);
    }

    public Rectangle getR() {
        return r;
    }

    public void setR(Rectangle r) {
        this.r = r;
    }
}
