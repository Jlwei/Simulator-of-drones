package layout;

import Bean.BuildingBean;
import Bean.DroneBean;
import Bean.PlantBean;
import Bean.StationBean;
import components.Building;
import components.Drone;
import components.Plant;
import components.Station;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class AboutRight {
    private static int i;
    private Boolean begin = false;
    private Boolean buildingHelper = false;
    private Boolean droneHelper = false;
    private Boolean stationHelper = false;
    private Boolean planteHelper = false;
    public static Text text2;

    private ArrayList<Node> nodes = new ArrayList<>();

    private AboutLeft aboutLeft;

    public AboutRight() {
        Line redLine = new Line(650, 0, 650, 800);
        redLine.setStroke(Color.RED);
        Main.root.getChildren().add(redLine);

        //building
        BuildingBean buildingBean = new BuildingBean(new Building(710, 20, 25, 25));

        Text text = new Text(700, 65, "building");
        Main.root.getChildren().add(text);
        Main.root.getChildren().add(buildingBean.getBuilding().getR());
        //drone
        DroneBean droneBean = new DroneBean(new Drone(725.0f, 100.0f, 15.0f));
        text = new Text(708, 130, "drone");
        Main.root.getChildren().add(text);
        Main.root.getChildren().add(droneBean.getDrone().getCircle());
        //plante
        PlantBean plantBean = new PlantBean(new Plant(0, 727, 175));
        plantBean.getPlant().setOnMouseDragged(e -> {
            plantBean.setPlante((int) e.getX(), (int) e.getY());

            plantBean.getPlant().setOnMouseReleased(e3 -> {
                if (planteHelper == false) {
                    plantBean.setX((int) e3.getX());
                    plantBean.setY((int) e3.getY());
                    plantBean.setColor(false);
                    plantBean.setHavePlant(false);
                    AboutLeft.planteArrayList.add(plantBean);
                }
                planteHelper = true;
            });
            plantBean.getPlant().setMouseTransparent(true);
            PlantBean bean = new PlantBean(new Plant(0, 727, 175));
            Main.root.getChildren().add(bean.getPlant());
            bean.getPlant().setOnMouseDragged(e2 -> {
                bean.setPlante((int) e2.getX(), (int) e2.getY());
                bean.getPlant().setMouseTransparent(true);
            });

            bean.getPlant().setOnMouseReleased(e3 -> {
                bean.setX((int) e3.getX());
                bean.setY((int) e3.getY());
                bean.setColor(false);
                bean.setHavePlant(false);
                AboutLeft.planteArrayList.add(bean);
            });
        });
        text = new Text(708, 190, "Plant");
        Main.root.getChildren().add(text);
        Main.root.getChildren().add(plantBean.getPlant());
        //station
        StationBean stationBean = new StationBean(new Station(710, 210, 30, 30, Color.BLUE));
        text = new Text(708, 260, "Station");
        Main.root.getChildren().add(text);
        Main.root.getChildren().add(stationBean.getStation().getR());

        buildingBean.getBuilding().getR().setOnMouseDragged(e -> {
            buildingBean.getBuilding().setR((int) e.getX(), (int) e.getY());

            buildingBean.getBuilding().getR().setOnMouseReleased(e3 -> {
                if (buildingHelper == false) {
                    buildingBean.setX((int) e3.getX());
                    buildingBean.setY((int) e3.getY());
                    buildingBean.setJudge(false);
                    buildingBean.setHavePlane(false);
                    AboutLeft.buildingBeanArrayList.add(buildingBean);
                    nodes.add(buildingBean.getBuilding().getR());
                }
                buildingHelper = true;
            });

            buildingBean.getBuilding().getR().setMouseTransparent(true);
            BuildingBean bean = new BuildingBean(new Building(710, 20, 25, 25));
            Main.root.getChildren().add(bean.getBuilding().getR());
            bean.getBuilding().getR().setOnMouseDragged(e2 -> {
                bean.getBuilding().setR((int) e2.getX(), (int) e2.getY());
                bean.getBuilding().getR().setMouseTransparent(true);
            });

            bean.getBuilding().getR().setOnMouseReleased(e3 -> {
                bean.setX((int) e3.getX());
                bean.setY((int) e3.getY());
                bean.setJudge(false);
                bean.setHavePlane(false);
                AboutLeft.buildingBeanArrayList.add(bean);
            });
        });

        droneBean.getDrone().getCircle().setOnMouseDragged(e -> {
            droneBean.getDrone().setCircle((int) e.getX(), (int) e.getY());

            droneBean.getDrone().getCircle().setOnMouseReleased(e3 -> {
                if (droneHelper == false) {
                    droneBean.setX((int) e3.getX());
                    droneBean.setY((int) e3.getY());
                    droneBean.setScoreXX((int) e3.getX() - 250);
                    droneBean.setScoreXY((int) e3.getX() + 250);
                    droneBean.setScoreYY((int) e3.getY() + 250);
                    droneBean.setScoreYX((int) e3.getY() - 250);
//                    System.out.println(droneBean.getX() + " " + droneBean.getY() + " " + droneBean.getScoreXX() + " " + droneBean.getScoreYX());
                    droneBean.setEle(10);
                    droneBean.setCharge(false);
                    droneBean.setFree(false);
                    droneBean.setFree2(false);
                    droneBean.setBuildingNumber(-1);
                    droneBean.setFree3(false);
//                    droneBean
                    AboutLeft.droneBeanArrayList.add(droneBean);
                }
                droneHelper = true;
            });

            droneBean.getDrone().getCircle().setMouseTransparent(true);
            DroneBean bean = new DroneBean(new Drone(725.0f, 100.0f, 15.0f));
            Main.root.getChildren().add(bean.getDrone().getCircle());
            bean.getDrone().getCircle().setOnMouseDragged(e2 -> {
                bean.getDrone().setCircle((int) e2.getX(), (int) e2.getY());
                bean.getDrone().getCircle().setMouseTransparent(true);
            });

            bean.getDrone().getCircle().setOnMouseReleased(e3 -> {
                bean.setX((int) e3.getX());
                bean.setY((int) e3.getY());
                bean.setScoreXX((int) e3.getX() - 250);
                bean.setScoreXY((int) e3.getX() + 250);
                bean.setScoreYY((int) e3.getY() + 250);
                bean.setScoreYX((int) e3.getY() - 250);
                bean.setEle(10);
                bean.setCharge(false);
                bean.setFree(false);
                bean.setFree2(false);
                bean.setBuildingNumber(-1);
                bean.setFree3(false);
                AboutLeft.droneBeanArrayList.add(bean);

            });
        });

        //station
        stationBean.getStation().getR().setOnMouseDragged(e -> {
            stationBean.getStation().setR((int) e.getX(), (int) e.getY());
            stationBean.getStation().getR().setMouseTransparent(true);
            stationBean.getStation().getR().setOnMouseReleased(e3 -> {
                if (stationHelper == false) {
                    stationBean.setFree(false);        //free status
                    stationBean.setX((int) e3.getX());
                    stationBean.setY((int) e3.getY());
                    AboutLeft.stationBeanArrayList.add(stationBean);
                }
                stationHelper = true;
            });

            StationBean bean = new StationBean(new Station(715, 210, 30, 30, Color.BLUE));
            Main.root.getChildren().add(bean.getStation().getR());
            bean.getStation().getR().setOnMouseDragged(e2 -> {
                bean.getStation().setR((int) e2.getX(), (int) e2.getY());
                bean.getStation().getR().setMouseTransparent(true);
            });

            bean.getStation().getR().setOnMouseReleased(e3 -> {
                bean.setFree(false);        //free status
                bean.setX((int) e3.getX());
                bean.setY((int) e3.getY());
                AboutLeft.stationBeanArrayList.add(bean);
            });
        });

        //Counter
        text = new Text(700, 460, "goods: ");
        Main.root.getChildren().add(text);
        text2 = new Text(760, 460, "0");
        Main.root.getChildren().add(text2);

        //Start button
        Button button = new Button("start");
        button.setLayoutX(698);
        button.setLayoutY(330);
        Main.root.getChildren().add(button);

        button.setOnAction(ex -> {
            AboutLeft.begin = false;
            aboutLeft = new AboutLeft();
        });

        //Pause button
        Button button2 = new Button("pause");
        button2.setLayoutX(698);
        button2.setLayoutY(360);
        button2.setOnAction(ex -> {
            AboutLeft.begin = true;
        });
        Main.root.getChildren().add(button2);
        //End button
        Button button3 = new Button("end");
        button3.setLayoutX(698);
        button3.setLayoutY(390);
        button3.setOnAction(ex -> {
            AboutLeft.begin = true;
            for (int i = 0; i < AboutLeft.droneBeanArrayList.size(); i++) {
                Main.root.getChildren().remove(AboutLeft.droneBeanArrayList.get(i).getDrone().getCircle());
            }

            for (int i = 0; i < AboutLeft.planteArrayList.size(); i++) {
                Main.root.getChildren().remove(AboutLeft.planteArrayList.get(i).getPlant());
            }

            for (int i = 0; i < AboutLeft.buildingBeanArrayList.size(); i++) {
                Main.root.getChildren().remove(AboutLeft.buildingBeanArrayList.get(i).getBuilding().getR());
                if (AboutLeft.buildingBeanArrayList.get(i).getHaveGoods() != null) {
                    Main.root.getChildren().remove(AboutLeft.buildingBeanArrayList.get(i).getHaveGoods().getGoods().getCircle());
                }
            }

            for (int i = 0; i < AboutLeft.stationBeanArrayList.size(); i++) {
                Main.root.getChildren().remove(AboutLeft.stationBeanArrayList.get(i).getStation().getR());
            }

            AboutLeft.stationBeanArrayList = new ArrayList<>();
            AboutLeft.buildingBeanArrayList = new ArrayList<>();
            AboutLeft.planteArrayList = new ArrayList<>();
            AboutLeft.droneBeanArrayList = new ArrayList<>();
            aboutLeft = new AboutLeft();
        });
        Main.root.getChildren().add(button3);
    }
}
