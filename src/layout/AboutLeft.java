package layout;

import Bean.*;
import Util.ChangeColorUtil;
import Util.ChargeUtil;
import Util.CompareUtil;
import components.Goods;
import javafx.application.Platform;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class AboutLeft {
    public static volatile ArrayList<BuildingBean> buildingBeanArrayList = new ArrayList<>();
    public static volatile ArrayList<DroneBean> droneBeanArrayList = new ArrayList<>();
    public static volatile ArrayList<StationBean> stationBeanArrayList = new ArrayList<>();
    public static volatile ArrayList<PlantBean> planteArrayList = new ArrayList<>();
    public static Boolean begin = false;
    public static volatile Integer count = 0;           //Counter

    public static Timer timer = new Timer();
    public static Timer timer2 = new Timer();
    public static Timer timer3 = new Timer();
    public static Timer timer4 = new Timer();

    public AboutLeft() {
        productionGoods();
        refluse();
        refluse2();
        refluse3();
        refluse4();
    }

    public void productionGoods() {
        int i = (int) (Math.random() * 3);
        if (i == 1 & !buildingBeanArrayList.isEmpty()) {
            int goodsNumber = (int) (Math.random() * buildingBeanArrayList.size());
            BuildingBean buildingBean = buildingBeanArrayList.get(goodsNumber);
//            System.out.println(goodsNumber + "  " + buildingBeanArrayList.size() + " " + buildingBean.getJudge());
            if (buildingBean.getJudge() == false) {
                GoodsBean goodsBean = new GoodsBean(new Goods(buildingBean.getX(), buildingBean.getY(), 15, Color.BLACK));
                Main.root.getChildren().add(goodsBean.getGoods().getCircle());
                buildingBean.setJudge(true);
                buildingBean.setHaveGoods(goodsBean);
                buildingBean.setHavePlane(false);
            }
        }
    }

    public void needCharge() {
        //减少一格电
        for (int i = 0; i < droneBeanArrayList.size(); i++) {
            int j = (int) (Math.random() * 2);
            if (j == 1) {
                DroneBean droneBean = droneBeanArrayList.get(i);
                droneBean.setEle(droneBean.getEle() - 1);
            }
        }
//        droneBeanArrayList.forEach(i -> System.out.print(i.getEle() + " "));
        ArrayList<DroneBean> droneBeans = new ArrayList<>();
        //   Screen out the drone that needs to be recharged
        for (int i = 0; i < droneBeanArrayList.size(); i++) {
            DroneBean droneBean = droneBeanArrayList.get(i);
            if (droneBean.getEle() <= 2 && droneBean.getFree2() == false)
                droneBeans.add(droneBean);
        }

        //  Sorting unmanned aerial vehicles


        if (!droneBeans.isEmpty()) {
            for (int i = 0; i < droneBeans.size(); i++) {
                //The property of delivery UAV is set to false here.
                int number = 0;
                double distance = 0, distance2 = 0;
                DroneBean droneBean = droneBeans.get(i);

                droneBean.setArrayList(new ArrayList<>());
                droneBean.setFree(false);
                if (droneBean.getBuildingNumber() != -1) {
                    buildingBeanArrayList.get(droneBean.getBuildingNumber()).setHavePlane(false);
                }
                ChargeUtil.findAStation(droneBean, stationBeanArrayList);
            }
        }

    }

    private void refluse() {
        timer = new Timer();
        try {
            timer.schedule(new TimerTask() {
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (begin == false) {
                                productionGoods();
                            }
                        }
                    });
                }
            }, 0, 2000);
        } catch (IllegalStateException i) {
            System.out.println("stop");
        }
    }

    private void refluse2() {
        timer2 = new Timer();
        try {
            timer2.schedule(new TimerTask() {
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (begin == false) {
                                needCharge();
                            }
                        }
                    });
                }
            }, 1000, 1000);
        } catch (IllegalStateException i) {
            System.out.println("stop");
        }
    }


    // The drone is wandering around
    private void refluse3() {
        timer3 = new Timer();
        try {
            timer3.schedule(new TimerTask() {
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (begin == false) {
                                CompareUtil.compare();
                                for (int i = 0; i < droneBeanArrayList.size(); i++) {
                                    DroneBean droneBean = droneBeanArrayList.get(i);
                                    go(droneBean, 16);
                                }
                            }
                        }
                    });
                }
            }, 0, 400);
        } catch (IllegalStateException i) {
            System.out.println("stop");
        }
    }


    //  Random discoloration of plants
    private void refluse4() {
        timer4 = new Timer();
        try {
            timer4.schedule(new TimerTask() {
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if (begin == false) {
                                ArrayList<PlantBean> list = new ArrayList<>();
                                //  Discoloration of plants at random
                                for (int i = 0; i < planteArrayList.size(); i++) {
                                    int random = (int) (Math.random() * 10);
                                    PlantBean plantBean = planteArrayList.get(i);
                                    if (random == 1 && plantBean.getColor() == false) {
                                        plantBean.getPlant().getPolygon().setFill(Color.RED);
                                        plantBean.setColor(true);
                                    }

                                    if (plantBean.getColor() == true && plantBean.getHavePlant() == false) {
                                        //  Add red plants to the array
//                                        System.out.print(i + " ");
                                        list.add(plantBean);
                                    }
                                }

                                //  If the plant becomes a red idle drone, look for him.
                                if (!list.isEmpty()) {
                                    ChangeColorUtil.changeColor(list);
                                }
                            }
                        }
                    });
                }
            }, 0, 1000);
        } catch (IllegalStateException i) {
            System.out.println("stop");
        }
    }

    public static void go(DroneBean droneBean, int speed) {
        ArrayList<Integer> arrayList = droneBean.getArrayList();
        ArrayList<Integer> arrayList2 = droneBean.getArrayList2();

        if ((arrayList == null || arrayList.isEmpty()) && (arrayList2 == null || arrayList2.isEmpty())) {
            int direction = (int) (Math.random() * speed + 1);
            if (direction == 1 && droneBean.getScoreXX() < droneBean.getX() - speed) {        //往左 right
                droneBean.setX(droneBean.getX() - speed);
            } else if (direction == 2 && droneBean.getScoreXY() > droneBean.getX() + speed) { //往右 left
                droneBean.setX(droneBean.getX() + speed);
            } else if (direction == 3 && droneBean.getScoreYX() < droneBean.getY() - speed) {      //往上  up
                droneBean.setY(droneBean.getY() - speed);
            } else if (direction == 4 && droneBean.getScoreYY() > droneBean.getY() + speed) {      //往下  down
                droneBean.setY(droneBean.getY() + speed);
            }
            droneBean.getDrone().setCircle(droneBean.getX(), droneBean.getY());
        } else if (droneBean.getFree2() == true) {          //  It means to recharge
            droneBean.setX(droneBean.getArrayList2().get(droneBean.getProx()));
            droneBean.setY(droneBean.getArrayList2().get(droneBean.getProy()));
            droneBean.getDrone().setCircle(droneBean.getArrayList2().get(droneBean.getProx()), droneBean.getArrayList2().get(droneBean.getProy()));
            droneBean.setProx(droneBean.getProx() + 2);
            droneBean.setProy(droneBean.getProy() + 2);
            if (droneBean.getProx() >= 19) {
                droneBean.setArrayList2(new ArrayList<>());
                droneBean.getStationBean().setFree(false);
                droneBean.setFree2(false);
                droneBean.setEle(10);
            }
        } else {
            droneBean.setX(droneBean.getArrayList().get(droneBean.getArrayX()));
            droneBean.setY(droneBean.getArrayList().get(droneBean.getArrayY()));
//            System.out.println(droneBean.getArrayList().get(droneBean.getArrayX()) + "  " + droneBean.getArrayList().get(droneBean.getArrayY()));
            droneBean.getDrone().setCircle(droneBean.getArrayList().get(droneBean.getArrayX()), droneBean.getArrayList().get(droneBean.getArrayY()));
            droneBean.setArrayX(droneBean.getArrayX() + 2);
            droneBean.setArrayY(droneBean.getArrayY() + 2);
            if (droneBean.getArrayX() >= 11) {
                if (droneBean.getFree3() == false) {
                    droneBean.setArrayList(new ArrayList<>());
                    buildingBeanArrayList.get(droneBean.getBuildingNumber()).setJudge(false);
                    droneBean.setFree(false);
                    buildingBeanArrayList.get(droneBean.getBuildingNumber()).setHavePlane(false);
                    Main.root.getChildren().remove(buildingBeanArrayList.get(droneBean.getBuildingNumber()).getHaveGoods().getGoods().getCircle());

                    count++;
                    AboutRight.text2.setText(String.valueOf(count));

                } else {
                    droneBean.setArrayList(new ArrayList<>());
                    droneBean.setFree(false);
                    droneBean.getPlantBean().setHavePlant(false);
                    droneBean.getPlantBean().setColor(false);
                    droneBean.getPlantBean().getPlant().getPolygon().setFill(Color.GREEN);
                }
            }
        }
    }
}
