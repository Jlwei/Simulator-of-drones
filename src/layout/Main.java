package layout;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

//All bool type false indicates idle, and true indicates that something is being done. The drone power is divided into 10 grades.
public class Main extends Application {
    public static Pane root = new Pane();

    @Override
    public void start(Stage primaryStage) {
        try {
            AboutRight aboutRight = new AboutRight();

            Scene scene = new Scene(root, 800, 800);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
