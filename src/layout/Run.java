package layout;

import java.util.ArrayList;

public class Run {
    private int x;
    private int y;
    private int fromX;
    private int fromY;
    private int domain;

    public Run(int x, int y, int fromX, int fromY, int domain) {
        this.x = x;
        this.y = y;
        this.fromX = fromX;
        this.fromY = fromY;
        this.domain = domain;
    }

    public ArrayList<Integer> move(Boolean bool) {
//        Map<Integer, Integer> map = new TreeMap<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        Integer nx = Math.abs(((x - fromX) / 6));
        Integer ny = Math.abs(((y - fromY) / 6));

        if (domain == 2) {
            for (int i = 0; i < 6; i++) {
                arrayList.add(x - (nx * (i + 1)));
                arrayList.add(y - (ny * (i + 1)));
                if (bool == true && i == 5) {
                    for (int j = 0; j < 5; j++) {
                        arrayList.add(x - (nx * (i + 1)) + (nx * j));
                        arrayList.add(y - (ny * (i + 1)) + (ny * j));
                    }
                } else if (bool == false && i == 5) {
                    for (int j = 0; j < 2; j++) {
                        arrayList.add(x - (nx * (i + 1)) + (nx * j));
                        arrayList.add(y - (ny * (i + 1)) + (ny * j));
                    }
                }
//                map.put(x - (nx * (i + 1)), y - (ny * (i + 1)));
            }
        } else if (domain == 3) {
            for (int i = 0; i < 6; i++) {
                arrayList.add(x - (nx * (i + 1)));
                arrayList.add(y + (ny * (i + 1)));
                if (bool == true && i == 5) {
                    for (int j = 0; j < 5; j++) {
                        arrayList.add(x - (nx * (i + 1)) + (nx * j));
                        arrayList.add(y + (ny * (i + 1)) - (ny * j));
                    }
                } else if (bool == false && i == 5) {
                    for (int j = 0; j < 2; j++) {
                        arrayList.add(x - (nx * (i + 1)) + (nx * j));
                        arrayList.add(y + (ny * (i + 1)) - (ny * j));
                    }
                }
//                map.put(x - (nx * (i + 1)), y + (ny * (i + 1)));
            }
        } else if (domain == 4) {
            System.out.print("....");
            for (int i = 0; i < 6; i++) {
                arrayList.add(x + (nx * (i + 1)));
                arrayList.add(y + (ny * (i + 1)));
                if (bool == true && i == 5) {
                    for (int j = 0; j < 5; j++) {
                        arrayList.add(x + (nx * (i + 1)) - (nx * j));
                        arrayList.add(y + (ny * (i + 1)) - (ny * j));
                    }
                } else if (bool == false && i == 5) {
                    for (int j = 0; j < 2; j++) {
                        arrayList.add(x + (nx * (i + 1)) - (nx * j));
                        arrayList.add(y + (ny * (i + 1)) - (ny * j));
                    }
                }
//                map.put(x + (nx * (i + 1)), y + (ny * (i + 1)));
            }
        } else if (domain == 5) {
//            System.out.println(x + " " + y + " " + fromX + " " + fromY);
            for (int i = 0; i < 6; i++) {
                int xx = x + (nx * (i + 1));
                int yy = y - (ny * (i + 1));
                arrayList.add(xx);
                arrayList.add(yy);
                if (bool == true && i == 5) {
                    for (int j = 0; j < 5; j++) {
                        arrayList.add(xx - (nx * j));
                        arrayList.add(yy + (ny * j));
                    }
                } else if (bool == false && i == 5) {
                    for (int j = 0; j < 2; j++) {
                        arrayList.add(xx - (nx * j));
                        arrayList.add(yy + (ny * j));
                    }
                }
//                System.out.println(x + (nx * (i + 1)) + " . " + (y - (ny * (i + 1))));
//                map.put(x + (nx * (i + 1)), y - (ny * (i + 1)));
            }
        }

        return arrayList;
    }
}
